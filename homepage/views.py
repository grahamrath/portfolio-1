from django.shortcuts import render
from homepage.models import Skills


def homepage(request):
    skills = Skills(request)
    context = {
        "skillslist": skills,
    }
    return render(request, "homepage.html", context)
